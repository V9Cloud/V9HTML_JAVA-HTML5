# V9HTML_JAVA-HTML5_企业互联集团订单管理-采购订单与销售订单

#### 项目介绍
V9HTML(JAVA+HTML5)企业互联集团订单管理（采购订单与销售订单）

HTML 5 是下一代的 HTML。
HTML5 仍处于完善之中。然而，大部分现代浏览器已经具备了某些 HTML5 支持。

HTML5 是 W3C 与 WHATWG 合作的结果。
编者注：W3C 指 World Wide Web Consortium，万维网联盟。
编者注：WHATWG 指 Web Hypertext Application Technology Working Group。
WHATWG 致力于 web 表单和应用程序，而 W3C 专注于 XHTML 2.0。在 2006 年，双方决定进行合作，来创建一个新版本的 HTML。
为 HTML5 建立的一些规则：
新特性应该基于 HTML、CSS、DOM 以及 JavaScript。
减少对外部插件的需求（比如 Flash）
更优秀的错误处理
更多取代脚本的标记
HTML5 应该独立于设备
开发进程应对公众透明

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)